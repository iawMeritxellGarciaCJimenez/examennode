var express = require("express");
var path = require("path");
var router = express.Router();
const alumnosController = require('../controllers/alumnos.js');
const docentesController = require('../controllers/docentes.js');
const asignaturaController = require('../controllers/asignaturas.js');


router.get("/", async (req, res, next) => {
    res.render("index.pug");
});

router.post("/asignatura", alumnosController.mostrar);
router.post("/asignatura", docentesController.mostrar);
router.post("/asignatura", async (req, res, next) => {
    res.render("newAsignatura.pug");
});

router.post("/asignatura/new", asignaturaController.crear);
router.post("/asignatura/update", asignaturaController.editar);
router.post("/asignatura/delete", asignaturaController.eliminar);
router.post("/asignatura/save", asignaturaController.listar);


module.exports = router;