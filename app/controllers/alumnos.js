var Alumno = require('../models/alumnos');

var mostrar = (req, res, next) => {
    Alumno.find({}, function(err, alumnos) {
        if(err) {
            return next(err);
        } else {
            res.json(alumnos);
        }
    });
};
module.exports = {mostrar};
