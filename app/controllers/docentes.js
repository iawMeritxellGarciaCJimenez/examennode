var Docente = require('../models/docente');

var mostrar = (req, res, next) => {
    Docente.find({}, function(err, docentes) {
        if(err) {
            return next(err);
        } else {
            res.json(docentes);
        }
    });
};
module.exports = {mostrar};