var Asignatura = require('../models/asignatura');

var listar = (req, res, next) => {
    Asignatura.find({}, function(err, asignaturas) {
        if(err) {
            return next(err);
        } else {
            res.json(asignaturas);
        }
    });
};

var crear = (req, res, next) => {
    var asignatura = new Asignatura({name: req.body.name, numHoras: req.body.numHoras, docente: req.body.docente});
    asignatura.save();
};

var editar = (req, res, next) => {
    // Asignatura.updateOne({ _id: req.body.id }, {
        
    // };
};

var eliminar = (req, res, next) => {
    Asignatura.findOneAndDelete({name: req.body.name, numHoras: req.body.numHoras, docente: req.body.docente});
};

module.exports = {listar, crear, editar, eliminar};
