var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var asignaturaSchema = new Schema({
    _id: Schema.Types.ObjectId,
    nombre: String,
    numHoras: String,
    docente: Schema.Types.ObjectId,
    alumnos: [Schema.Types.ObjectId]
});

module.exports = mongoose.model('Asignatura', asignaturaSchema);
