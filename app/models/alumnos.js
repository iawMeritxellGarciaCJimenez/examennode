var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var alumnoSchema = new Schema({
    _id: Schema.Types.ObjectId,
    nombre: String,
    apellido: String
});

module.exports = mongoose.model('Alumno', alumnoSchema);
